moves = open("inputs/2.txt").read().split('\n')

x = 0
y = 0

for move in moves:
    if 'forward' in move:
        x += int(move[-1])

    if 'down' in move:
        y += int(move[-1])
        
    if 'up' in move:
        y -= int(move[-1])

print(x*y)