depths = open("inputs/1.txt").read().split('\n')

previous = depths[0]
current = 0
total = 1

for depth in depths[1:]:
    current = depth
    if current > previous:
        total += 1
    previous = current

print(total)