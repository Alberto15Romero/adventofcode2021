depths = open("inputs/1.txt").read().split('\n')
A = depths[0]
B = depths[1]
C = depths[2]

total = 0
prevWindowSum = int(A) + int(B) + int(C)

for depth in depths[3:]:
    A = B
    B = C
    C = depth
    currentWindowSum = int(A) + int(B) + int(C)
    
    if currentWindowSum > prevWindowSum:
        total += 1
    prevWindowSum = currentWindowSum

print(total)