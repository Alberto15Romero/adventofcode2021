lines = open("inputs/3.txt").read().split('\n')


freq = {1:[0, 0], 2:[0, 0], 3:[0, 0], 4:[0, 0], 5:[0, 0], 6:[0, 0], 7:[0, 0], 8:[0, 0], 9:[0, 0], 10:[0, 0], 11:[0, 0], 12:[0, 0]}
# epsilon = {1:(0, 0), 2:(0, 0), 3:(0, 0), 4:(0, 0), 5:(0, 0), 6:(0, 0), 7:(0, 0), 8:(0, 0), 9:(0, 0), 10:(0, 0), 11:(0, 0), 12:(0, 0)}

for line in lines:
    counter = 0
    for bit in line:
        if bit == '0':
            freq[counter + 1][0] += 1
        if bit == '1':
            freq[counter + 1][1] += 1
        counter += 1
        counter %= 12
print(freq)

gamma = str(max(zip(freq[1], range(len(freq[1]))))[1]) + str(max(zip(freq[2], range(len(freq[2]))))[1]) + str(max(zip(freq[3], range(len(freq[3]))))[1]) + \
        str(max(zip(freq[4], range(len(freq[4]))))[1]) + str(max(zip(freq[5], range(len(freq[5]))))[1]) + str(max(zip(freq[6], range(len(freq[6]))))[1]) + \
        str(max(zip(freq[7], range(len(freq[7]))))[1]) + str(max(zip(freq[8], range(len(freq[8]))))[1]) + str(max(zip(freq[9], range(len(freq[9]))))[1]) + \
        str(max(zip(freq[10], range(len(freq[10]))))[1]) + str(max(zip(freq[11], range(len(freq[11]))))[1]) + str(max(zip(freq[12], range(len(freq[12]))))[1]) 

epsilon = str(min(zip(freq[1], range(len(freq[1]))))[1]) + str(min(zip(freq[2], range(len(freq[2]))))[1]) + str(min(zip(freq[3], range(len(freq[3]))))[1]) + \
        str(min(zip(freq[4], range(len(freq[4]))))[1]) + str(min(zip(freq[5], range(len(freq[5]))))[1]) + str(min(zip(freq[6], range(len(freq[6]))))[1]) + \
        str(min(zip(freq[7], range(len(freq[7]))))[1]) + str(min(zip(freq[8], range(len(freq[8]))))[1]) + str(min(zip(freq[9], range(len(freq[9]))))[1]) + \
        str(min(zip(freq[10], range(len(freq[10]))))[1]) + str(min(zip(freq[11], range(len(freq[11]))))[1]) + str(min(zip(freq[12], range(len(freq[12]))))[1]) 

gamma = int(gamma, 2)
epsilon = int(epsilon, 2)
print(f"Gamma: {gamma}")
print(f"Epsilon: {epsilon}")
print(f"Power: {int(gamma)*int(epsilon)}")